import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { JobsComponent } from './view-jobs/jobs.component';
import { ApplyComponent } from './apply/apply.component';
import { SignupComponent } from './signup/signup.component';
import { AuthGuardService } from './services/auth-guard.service';
import { AddJobComponent } from './add-job/add-job.component';
import { ViewApplicantsComponent } from './view-applicants/view-applicants.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { CareersComponent } from './careers/careers.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuardService] },
  { path: 'jobs/view', component: JobsComponent, canActivate: [AuthGuardService] },
  { path: 'job/apply/:jobId', component: ApplyComponent, canActivate: [AuthGuardService] },
  { path: 'job/add', component: AddJobComponent, canActivate: [AuthGuardService] },
  { path: 'job/update/:jobId', component: AddJobComponent, canActivate: [AuthGuardService] },
  { path: 'company/jobs', component: JobsComponent, canActivate: [AuthGuardService] },
  { path: 'applicants/:postId', component: ViewApplicantsComponent, canActivate: [AuthGuardService] },
  { path: 'contactus', component: ContactUsComponent, canActivate: [AuthGuardService] },
  { path: 'careers', component: CareersComponent, canActivate: [AuthGuardService] },

  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ LoginComponent, SignupComponent, HomeComponent, JobsComponent, ApplyComponent, AddJobComponent, ViewApplicantsComponent, PageNotFoundComponent ]
