import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JobsService } from '../services/jobs.service';
import { IJob } from '../interface/job';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent implements OnInit { 
  public jobsResponse : IJob[];
  public isError : boolean;
  public msg : string;
  public errorType : string;
  public page : number;
  public isLastPage : boolean = false;
  public userType : string;

  private userId : string;
  private isFilterEnabled : boolean;
  private filterType : string;
  private limit : number = 10;

  constructor(private _jobsService : JobsService, private router:Router) { }

  ngOnInit(): void {
    const current : Date = new Date();
    const timestamp = current.getTime();
    this.userType = localStorage.getItem('userType');
    this.userId = localStorage.getItem('userId');
    this.getJobs(timestamp);
    this.page = 1;
  }

  getJobs(timestamp : number) {
    if (this.userType == "STUDENT") {
      this.jobs(timestamp)
    } else if (this.userType == "COMPANY") {
      this.companyJobs(timestamp)
    }
  }

  jobs(timestamp : number) {
    this._jobsService.getJobs(timestamp, this.limit).subscribe((data: any) => {
      if (!data.hasOwnProperty('error')) {
        if(data.posts == undefined) {
          this.isLastPage = true;
          this.page--;
        } else {
          this.jobsResponse = data.posts;
          localStorage.setItem('firstDate', this.jobsResponse[0].createdAt.toString());
          localStorage.setItem('lastDate', this.jobsResponse[this.jobsResponse.length -1].createdAt.toString());
        }
      } else {
        this.setError(data);
      }
    });
  }

  companyJobs(timestamp : number) {
    this._jobsService.getCompanyJobs(this.userId, timestamp, this.limit).subscribe((data: any) => {
      if (!data.hasOwnProperty('error')) {
        if(data.posts == undefined) {
          this.isLastPage = true;
          this.page--;
        } else {
          this.jobsResponse = data.posts;
          localStorage.setItem('firstDate', this.jobsResponse[0].createdAt.toString());
          localStorage.setItem('lastDate', this.jobsResponse[this.jobsResponse.length -1].createdAt.toString());
        }
      } else {
        this.setError(data);
      }
    });
  }

  useFilter(filterType : string) {
    localStorage.removeItem('firstDate');
    localStorage.removeItem('lastDate');
    this.page = 1;
    this.isLastPage = false;
    this.filterType = filterType;
    this.filter(this.filterType, 0);
  }

  pageChange(state : string) {
    let timestamp : number;
    if(state == "next") {
      timestamp = +localStorage.getItem('lastDate');
      this.page++;
    } else if(state == "previous") {
      timestamp = +localStorage.getItem('firstDate');
      this.page--;
    }
    if(this.isFilterEnabled) {
      this.filter(this.filterType, timestamp);
    } else {
      this.getJobs(timestamp);
    }
  }

  filter(filterType : string, timestamp : number) {
    if(timestamp == 0) {
      const current : Date = new Date();
      timestamp = current.getTime();
    }
    if (filterType != "NONE") {
      this.isFilterEnabled = true;
      this.filterJobs(filterType, timestamp);
    } else {
      this.isFilterEnabled = false;
      this.getJobs(timestamp);
    }
  }

  filterJobs(filterType : string, timestamp : number) {
    this._jobsService.filterJobs(filterType, timestamp, this.limit).subscribe((data: any) => {
      if (!data.hasOwnProperty('error')) {
        if(data.posts == undefined) {
          this.isLastPage = true;
          this.page--;
        } else {
          this.jobsResponse = data.posts;
          localStorage.setItem('firstDate', this.jobsResponse[0].createdAt.toString());
          localStorage.setItem('lastDate', this.jobsResponse[this.jobsResponse.length -1].createdAt.toString());
        }
      } else {
        this.setError(data);
      }
    });
  }

  setError(data: IJob) {
    this.isError = data.error;
    this.msg = data.msg;
    this.errorType = data.errorCode;
  }

  apply(jobId: string) {
    this.router.navigate(['/job/apply/' + jobId]);
  }

  viewApplicant(jobId: string) {
    this.router.navigate(['/applicants/' + jobId]);
  }

  edit(jobId: string) {
    this.router.navigate(['/job/update/' + jobId]);
    // this._jobsService.filterJobs(filterType, timestamp, this.limit).subscribe((data: any) => {
    //   if (!data.hasOwnProperty('error')) {
    //     if(data.posts == undefined) {
    //       this.isLastPage = true;
    //       this.page--;
    //     } else {
    //       this.jobsResponse = data.posts;
    //       localStorage.setItem('firstDate', this.jobsResponse[0].createdAt.toString());
    //       localStorage.setItem('lastDate', this.jobsResponse[this.jobsResponse.length -1].createdAt.toString());
    //     }
    //   } else {
    //     this.setError(data);
    //   }
    // });
  }

  delete(jobId: string) {
    this._jobsService.deleteJob(jobId).subscribe((data: any) => {
      if (!data.hasOwnProperty('error')) {
        this.router.navigate(['/jobs/view/']);
      } else {
        this.setError(data);
      }
    });
  }
}
