import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { ILoginResponse } from '../interface/loginresponse';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public isError = false;
  public loginRequest: any = {};
  public loginResponse : any = {};
  public msg: string;
  public errorType: string;
  public token: string;
  public user: any = {};

  public password: string = '';

  constructor(private _loginservice: LoginService, private router:Router) {}

  ngOnInit(): void {}

  public requestLogin() {
    this.loginRequest = {
      email: (<HTMLInputElement>document.getElementById('username')).value,
      password: (<HTMLInputElement>document.getElementById('password')).value
    }
    this._loginservice.login(this.loginRequest).subscribe((data) => {
      this.loginResponse = data;
      if (!this.loginResponse.hasOwnProperty('error')) {
        this.setResponse(data);
      } else {
        this.setError(data);
        this.password = '';
      }
    });
  }

  setResponse(data: ILoginResponse) {
    this.token = data.loginResponse.token;
    this.user = data.loginResponse.user;

    localStorage.setItem('token', this.token);
    localStorage.setItem('email', this.user.email);
    localStorage.setItem('userId', this.user.userId);
    localStorage.setItem('userType', this.user.userType);

    this.router.navigate(['/home/']);
  }

  setError(data: ILoginResponse) {
    this.isError = data.error;
    this.msg = data.msg;
    this.errorType = data.errorCode;
  }
}
