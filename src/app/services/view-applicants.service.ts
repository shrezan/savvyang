import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { IApplicant } from '../interface/applicant';

@Injectable({
  providedIn: 'root'
})
export class ViewApplicantsService {

  reqHeader = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    Authorization: `${localStorage.getItem('token')}`,
  };

  constructor(private http: HttpClient) {}

  getApplicants(postId : string, timestamp : number, limit : number): any {
    return this.http
      .get('http://localhost:8000/apply/' + postId + '/' + timestamp + '/' + limit, { headers: this.reqHeader })
      .pipe(
        map((data: IApplicant[]) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || null);
  }
}
