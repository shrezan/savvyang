import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ILoginResponse } from '../interface/loginresponse';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  reqHeader = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  };

  constructor(private http: HttpClient) {}

  login(request: any) {
    return this.http
      .post<ILoginResponse>('http://localhost:8000/account/login', request, {
        headers: this.reqHeader,
      })
      .pipe(
        map((data: ILoginResponse) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  logout(): Observable<boolean> {
    return this.http
      .get<any>('http://localhost:8000/account/logout', {
        headers: {
          Authorization: localStorage.getItem('token'),
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      })
      .pipe(
        map((data) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || null);
  }
}
