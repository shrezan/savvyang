import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApplyService {
  reqHeader = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    Authorization: `${localStorage.getItem('token')}`,
  };

  constructor(private http: HttpClient) {}

  apply(request: any) {
    return this.http
      .post<any>('http://localhost:8000/apply', request, {
        headers: this.reqHeader,
      })
      .pipe(
        map((data) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || null);
  }
}
