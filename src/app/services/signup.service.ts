import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ISignupResponse } from '../interface/signupresponse';

@Injectable({
  providedIn: 'root',
})
export class SignupService {
  reqHeader = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  };

  constructor(private http: HttpClient) {}

  signup(request: any) {
    return this.http
      .post<ISignupResponse>('http://localhost:8000/user', request, {
        headers: this.reqHeader,
      })
      .pipe(
        map((data: ISignupResponse) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || null);
  }
}
