import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpEvent,
  HttpErrorResponse,
  HttpEventType,
} from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { IUpload } from '../interface/upload';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UploadService {
  reqHeader = {
    Accept: 'application/json',
    Authorization: `${localStorage.getItem('token')}`,
  };

  constructor(private httpClient: HttpClient) {}

  public sendFormData(formData) {
    return this.httpClient
      .post<IUpload>('http://localhost:8000/upload', formData, {
        headers: this.reqHeader,
        reportProgress: true,
      })
      .pipe(
        map((data: IUpload) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || null);
  }
}
