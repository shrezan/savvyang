import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { IJob } from '../interface/job';
import { IJobDesc } from '../interface/job-desc';

@Injectable({
  providedIn: 'root',
})
export class JobsService {
  
  reqHeader = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    Authorization: `${localStorage.getItem('token')}`,
  };

  constructor(private http: HttpClient) {}

  getJobs(timestamp : number, limit : number): any {
    return this.http
      .get('http://localhost:8000/jobs' + '/' + timestamp + '/' + limit, { headers: this.reqHeader })
      .pipe(
        map((data: IJob) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  getCompanyJobs(companyId : string, timestamp : number, limit : number): any {
    return this.http
      .get('http://localhost:8000/company/posts/' + companyId + '/' + timestamp + '/' + limit, { headers: this.reqHeader })
      .pipe(
        map((data: IJob) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  getJobById(jobId : string): any {
    return this.http
      .get('http://localhost:8000/job/' + jobId, { headers: this.reqHeader })
      .pipe(
        map((data: IJobDesc) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  filterJobs(jobType : string, timestamp : number, limit : number): any {
    return this.http
      .get('http://localhost:8000/jobs/' + jobType + '/' + timestamp + '/' + limit, { headers: this.reqHeader })
      .pipe(
        map((data: IJob) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  deleteJob(jobId : string): any {
    return this.http
      .delete('http://localhost:8000/job/' + jobId, { headers: this.reqHeader })
      .pipe(
        map((data: IJob) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || null);
  }
}
