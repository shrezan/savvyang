import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { UploadService } from '../services/upload.service';
import { IUpload } from '../interface/upload';
import { JobsService } from '../services/jobs.service';
import { IJobDesc } from '../interface/job-desc';
import { ApplyService } from '../services/apply.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-apply',
  templateUrl: './apply.component.html',
  styleUrls: ['./apply.component.css'],
})
export class ApplyComponent implements OnInit {
  @ViewChild('fileUpload', { static: false }) fileUpload: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private uploadService: UploadService,
    private _jobsService: JobsService,
    private _applyService: ApplyService
  ) {}

  public files = [];
  private routeSub: Subscription;
  private jobId: string;
  private cvLink: string;

  public postUser: IJobDesc;
  public isError: boolean;
  public msg: string;
  public errorType: string;
  public applyRequest: any = {};
  public uploadResponse: IUpload;
  public loading = false;
  public isSaved = false;
  public result = false;
  public canApply = false;
  public applyResponse: boolean = false;
  public applySuccess: boolean = false;

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe((params) => {
      this.jobId = params['jobId'];
    });
    this.getJobById(this.jobId);
  }

  getJobById(jobId : string) {
    this._jobsService.getJobById(jobId).subscribe((data) => {
      if (!data.hasOwnProperty('error')) {
        this.postUser = data.postUser;
        console.log(this.postUser);
        console.log(this.postUser.post);
        console.log(this.postUser.user);
      } else {
        this.setError(data);
      }
    });
  }

  sendFile(file) {
    const formData = new FormData();
    formData.append('file', file.data);
    file.inProgress = true;
    this.uploadService.sendFormData(formData).subscribe((data: IUpload) => {
      console.log(event);
      if (data.error) {
        this.loading = false;
        this.isSaved = false;
        this.result = true;
        this.msg = data.msg;
      } else {
        this.loading = false;
        this.isSaved = true;
        this.result = true;
        this.canApply = true;
        this.cvLink = data.url;
      }
      console.log(data);
      console.log(this.cvLink);
    });
  }

  private sendFiles() {
    this.fileUpload.nativeElement.value = '';
    this.files.forEach((file) => {
      this.sendFile(file);
    });
  }

  onClick() {
    this.loading = true;
    const fileUpload = this.fileUpload.nativeElement;
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files.push({ data: file, inProgress: false, progress: 0 });
      }
      this.sendFiles();
    };
    fileUpload.click();
  }

  onApply() {
    this.loading = true;
    this.applyRequest = {
      studentId: localStorage.getItem('userId'),
      postId: this.jobId,
      cvLink: this.cvLink
    };

    this._applyService.apply(this.applyRequest).subscribe((data) => {
      if (!data.hasOwnProperty('error')) {
        this.applySuccess = true;
      } else {
        this.applySuccess = false;
        this.msg = data.msg;
      }

      this.applyResponse = true;
      this.loading = false;
    });
  }

  setError(data: IJobDesc) {
    this.isError = data.error;
    this.msg = data.msg;
    this.errorType = data.errorCode;
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }
}
