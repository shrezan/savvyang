import { Component } from '@angular/core';
import { LoginService } from './services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private _loginservice: LoginService, private router:Router) {}

  isAuthorized() {
    return localStorage.getItem("token") != null;
  }

  isCompany() {
    return localStorage.getItem("userType") === "COMPANY";
  }

  logout() {
    console.log("trying to logout");
    this._loginservice.logout().subscribe((data) => {
      if (!data.hasOwnProperty('error')) {
        localStorage.clear();
        this.router.navigate(['/login']);
      } else {
        console.log("Try logging out again.")
      }
    });
  }
}
