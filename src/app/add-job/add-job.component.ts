import { Component, OnInit } from '@angular/core';
import { AddJobService } from '../services/add-job.service';
import { IAddJobResponse } from '../interface/add-job';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-job',
  templateUrl: './add-job.component.html',
  styleUrls: ['./add-job.component.css']
})
export class AddJobComponent implements OnInit {

  constructor(private _addjobservice: AddJobService, private router: Router) {}

  ngOnInit(): void {}

  public addJobRequest: any = {};
  public addJobResponse: any = {};
  public title: string;
  public postType: string;
  public description: string;

  public isTitleValid: boolean = true;
  public isJobTypeValid: boolean = true;
  public isDescriptionValid: boolean = true;
  
  public isError: boolean;
  public msg: string;
  public errorType: string;

  onSubmit() {
    this.isTitleValid = true;
    this.isJobTypeValid = true;
    this.isDescriptionValid = true;

    this.checkValidation();

    if(this.isTitleValid && this.isJobTypeValid && this.isDescriptionValid) {
      this._addjobservice.addJob(this.addJobRequest).subscribe((data) => {
        this.addJobResponse = data;
        if (!this.addJobResponse.hasOwnProperty('error')) {
          this.setResponse();
          this.router.navigate(['/jobs/view']);
        } else {
          this.setError(data);
        }
      });
    }
  }

  onView() {
    this.router.navigate(['/company/jobs']);
  }

  checkValidation() {
    this.title = (<HTMLInputElement>document.getElementById('title')).value;
    this.postType = (<HTMLInputElement>document.getElementById('post-type')).value;
    this.description = (<HTMLInputElement>document.getElementById('description')).value;
    if (this.title == '') {
      this.isTitleValid = false;
    }
    if (this.postType == "NONE") {
      this.isJobTypeValid = false;
    }
    if (this.description == '') {
      this.isDescriptionValid = false;
    }
    this.addJobRequest = {
      title: this.title,
      postType: this.postType,
      description: this.description,
      companyId: localStorage.getItem('userId')
    };
    
  }

  setResponse() {
    alert("New job added.");
  }

  setError(data: IAddJobResponse) {
    this.isError = data.error;
    this.msg = data.msg;
    this.errorType = data.errorCode;
  }
}
