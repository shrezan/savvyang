import { Component, OnInit } from '@angular/core';
import { SignupService } from '../services/signup.service';
import { Router } from '@angular/router';
import { ISignupResponse } from '../interface/signupresponse';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  constructor(private _signupservice: SignupService, private router:Router) {}

  ngOnInit(): void {}

  public signupRequest: any = {};
  public signupResponse: any = {};
  public email: string;
  public password: string;
  public fullName: string;
  public phone: string;
  public address: string;
  public qualification: string;
  public userType: string = "COMPANY";
  public type : number;

  public isPasswordValid: boolean = true;
  public isRePasswordValid: boolean = true;
  public isFullNameValid: boolean = true;
  public isEmailValid: boolean = true;
  public isQualificationValid : boolean = true;
  public options: number = 1;

  
  public isError: boolean;
  public msg: string;
  public errorType: string;

  onItemChange(event){
    this.userType = event.target.value;
  }

  onSubmit() {
    this.isPasswordValid = true;
    this.isRePasswordValid = true;
    this.isFullNameValid = true;
    this.isEmailValid = true;
    this.isQualificationValid = true;

    this.checkValidation();

    if(this.isPasswordValid && this.isRePasswordValid && this.isFullNameValid && this.isEmailValid && this.isQualificationValid) {
      this._signupservice.signup(this.signupRequest).subscribe((data) => {
        this.signupResponse = data;
        if (!this.signupResponse.hasOwnProperty('error')) {
          this.setResponse();
        } else {
          this.setError(data);
        }
      });
    }
  }

  checkValidation() {
    this.email = (<HTMLInputElement>document.getElementById('email')).value;
    this.password = (<HTMLInputElement>document.getElementById('password')).value;
    this.fullName = (<HTMLInputElement>document.getElementById('fullName')).value;
    this.phone = (<HTMLInputElement>document.getElementById('phone')).value;
    this.address = (<HTMLInputElement>document.getElementById('address')).value;
    if (this.fullName.length < 5) {
      this.isFullNameValid = false;
    }
    if (this.password.length < 5) {
      this.isPasswordValid = false;
    }
    if (this.email.length < 5 || !this.email.includes('@') || !this.email.includes('.')) {
      this.isEmailValid = false;
    }
    if (this.password != (<HTMLInputElement>document.getElementById("re-password")).value) {
      this.isRePasswordValid = false;
    }
    if (this.type == 1) {
      this.qualification = (<HTMLInputElement>document.getElementById('qualification')).value;
      if(this.qualification == "NONE") {
        this.isQualificationValid = false;
      } else {
        this.signupRequest = {
          user: {
            name: this.fullName,
            phone: this.phone,
            email: this.email,
            address: this.address,
            qualification: this.qualification,
            userType: this.userType
          },
          password: this.password
        };
      } 
    } else {
      this.signupRequest = {
        user: {
          name: this.fullName,
          phone: this.phone,
          email: this.email,
          address: this.address,
          userType: this.userType
        },
        password: this.password
      };
    }
  }

  setResponse() {
    alert("Congratulations! You've created your account.");
    this.router.navigate(['/login/']);
  }

  setError(data: ISignupResponse) {
    this.isError = data.error;
    this.msg = data.msg;
    this.errorType = data.errorCode;
  }
}
