export interface IUpload {
  error : boolean;
  msg : string;
  errorCode : string;
  path : string;
  url : string;
}