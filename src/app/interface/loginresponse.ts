import { IUser } from './user'

export interface ILoginResponse {
  error : boolean;
  msg : string;
  errorCode : string;
  loginResponse : {
    token : string;
    user : IUser;
  }
}