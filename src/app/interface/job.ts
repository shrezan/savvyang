export interface IJob {
  error : boolean;
  msg : string;
  errorCode : string;
  postId : string;
  companyId: string;
  title : string;
  description : string;
  postType : string;
  createdAt : number;
  updatedAt : number;
}