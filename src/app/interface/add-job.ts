
export interface IAddJobResponse {
  error : boolean;
  msg : string;
  errorCode : string;
}