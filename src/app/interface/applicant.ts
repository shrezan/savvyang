import {IUser} from "./user"

export interface IApplicant {
  error : boolean;
  msg : string;
  errorCode : string;
  postId : string;
  cvLink: string;
  user : IUser;
  createdAt : number;
  updatedAt : number;
}