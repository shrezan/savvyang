export interface IJobDesc {
  error : boolean;
  msg : string;
  errorCode : string;
  post : {
    postId : string;
    companyId: string;
    title : string;
    description : string;
    postType : string;
    createdAt : number;
    updatedAt : number
  }
  user : {
    name : string;
    email : string;
    address : string;
  }
  
}