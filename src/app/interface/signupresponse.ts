import { IUser } from './user'

export interface ISignupResponse {
  error : boolean;
  msg : string;
  errorCode : string;
  user : IUser;
}