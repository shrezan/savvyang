export interface IUser {
  userId: string;
  email: string;
  address: string;
  phone: string;
  name: string;
  userType: string;
  qualification: string;
  createdAt: number;
  updatedAt: number;
}