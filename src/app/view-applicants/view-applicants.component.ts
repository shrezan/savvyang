import { Component, OnInit } from '@angular/core';
import { ViewApplicantsService } from '../services/view-applicants.service';
import { IApplicant } from '../interface/applicant';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-view-applicants',
  templateUrl: './view-applicants.component.html',
  styleUrls: ['./view-applicants.component.css']
})
export class ViewApplicantsComponent implements OnInit {

  public applicantsResponse : IApplicant[];
  public isError : boolean;
  public msg : string;
  public errorType : string;
  public page : number;
  public isLastPage : boolean = false;
  public userType : string;
  public trustedUrl: SafeUrl;
  public cvLink : string;

  private routeSub: Subscription;
  private limit : number = 20;
  private postId : string;

  constructor(
    private _applicantsService : ViewApplicantsService, 
    private route: ActivatedRoute,
    protected _sanitizer: DomSanitizer,
    private router : Router) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe((params) => {
      this.postId = params['postId'];
    });
    const current : Date = new Date();
    const timestamp = current.getTime();
    this.getApplicants(this.postId, timestamp);
    this.page = 1;
  }

  getApplicants(postId : string, timestamp : number) {
    this._applicantsService.getApplicants(postId, timestamp, this.limit).subscribe((data: any) => { 
      if (!data.hasOwnProperty('error')) {
        if(data.applies == undefined) {
          this.isLastPage = true;
          this.page--;
        } else {
          this.applicantsResponse = data.applies;
          localStorage.setItem('firstDate', this.applicantsResponse[0].createdAt.toString());
          localStorage.setItem('lastDate', this.applicantsResponse[this.applicantsResponse.length -1].createdAt.toString());
        }
      } else {
        this.setError(data);
      }
    });
  }

  goToLink(url: string) {
    window.open(url, "_blank");
  }

  pageChange(state : string) {
    let timestamp : number;
    if(state == "next") {
      timestamp = +localStorage.getItem('lastDate');
      this.page++;
    } else if(state == "previous") {
      timestamp = +localStorage.getItem('firstDate');
      this.page--;
    }
    this.getApplicants(this.postId, timestamp);
  }

  setError(data: IApplicant) {
    this.isError = data.error;
    this.msg = data.msg;
    this.errorType = data.errorCode;
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

}
