import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { JobsComponent } from './view-jobs/jobs.component';
import { ApplyComponent } from './apply/apply.component';
import { SignupComponent } from './signup/signup.component';

import { LoginService } from './services/login.service';
import { JobsService } from './services/jobs.service';
import { AddJobComponent } from './add-job/add-job.component';
import { ViewApplicantsComponent } from './view-applicants/view-applicants.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { CareersComponent } from './careers/careers.component';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    PageNotFoundComponent,
    JobsComponent,
    ApplyComponent,
    SignupComponent,
    AddJobComponent,
    ViewApplicantsComponent,
    ContactUsComponent,
    CareersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  exports: [],
  providers: [LoginService, JobsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
